import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import AccountScreen from "../screens/AccountScreen";
import AccountDetailsScreen from "../screens/AccountDetailsScreen";
import UserReviewsScreen from "../screens/UserReviewsScreen";

const Stack = createStackNavigator();

const Account = () => (
  <Stack.Navigator>
    <Stack.Screen name="AccountScreen" component={AccountScreen}/>
    <Stack.Screen name="AccountDetails" component={AccountDetailsScreen}/>
    <Stack.Screen name="UserReviews" component={UserReviewsScreen} />
    </Stack.Navigator>
);

export default Account;
