import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import UserReviewEditScreen from "../screens/UserReviewEditScreen";
import UserReviewScreen from "../screens/UserReviewsScreen";

const Stack = createStackNavigator();

const UserReviews = () => (
  <Stack.Navigator mode="modal" screenOptions={{ headerShown: false }}>
    <Stack.Screen name="UserReviews"component={UserReviewScreen}
    />
    <Stack.Screen name="UserReviewEditScreen" component={UserReviewEditScreen}
    />
  </Stack.Navigator>
);

export default UserReviews;
