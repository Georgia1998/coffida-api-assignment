import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import LocationDetails from "../screens/LocationDetailsScreen";
import ReviewFormComponent from "../components/lists/ReviewFormComponent";

const Stack = createStackNavigator();

const LocationDetailsNavigator = () => (
  <Stack.Navigator mode="modal" screenOptions={{ headerShown: false }}>
    <Stack.Screen name="LocationsDetails" component={LocationDetails}/>
    <Stack.Screen name="ReviewFormComponent" component={ReviewFormComponent} />
    </Stack.Navigator>
);

export default LocationDetailsNavigator;
