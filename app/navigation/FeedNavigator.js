import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import LocationsScreen from "../screens/LocationsScreen";
import LocationDetailsNavigator from "../navigation/LocationDetailsNavigator";

const Stack = createStackNavigator();

const FeedNavigator = () => (
  <Stack.Navigator mode="modal" screenOptions={{ headerShown: false }}>
    <Stack.Screen name="Locations" component={LocationsScreen}/>
    <Stack.Screen name="LocationsDetails" component={LocationDetailsNavigator} />
  </Stack.Navigator>
);

export default FeedNavigator;
