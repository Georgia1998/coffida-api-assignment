export default Object.freeze({
  LOCATION_DETAILS: "LocationsDetails",
  FAVOURITES_LIST: "FavouritesScreen",
  ACCOUNT_DETAILS: "AccountDetailsScreen",
  ACCOUNT_SCREEN: "AccountScreen",
  REVIEW_EDIT: "ReviewFormComponent",
  USER_REVIEWS: "UserReviewsScreen",
  USER_REVIEW_EDIT: "UserReviewEditScreen",
  LOGIN: "Login",
  REGISTER: "Register",
});
