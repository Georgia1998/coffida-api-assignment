import { useContext } from "react";

import AuthContext from "./context";
import authStorage from "./storage";

export default useAuth = () => {
  const { user, setUser } = useContext(AuthContext);

  const logIn = (user,authToken) => {
    if (authToken) {
      authStorage.storeToken(authToken);
      setUser(user);
    } else {
      console.log("failed to verify auth token");
    }
  };

  const logOut = () => {
    setUser(null);
    authStorage.removeToken();
  };

  return { user, logIn, logOut};
};
