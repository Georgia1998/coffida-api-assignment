import React,  { useState } from "react";
import { View, StyleSheet, FlatList, ScrollView } from "react-native";
import { Image } from "react-native-expo-image-cache";
import { Button } from "react-native-elements";
import {
  ErrorMessage,
} from "../components/forms";

import colors from "../config/colors";
import Text from "../components/Text";
import ReviewCard from "../components/ReviewCard";
import routes from "../navigation/routes";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import locationApi from "../api/locations";

function LocationDetailsScreen({ route, navigation }) {
  console.log("reviews");
  console.log(route.params);
  const location = route.params;
  const [favouriteFailed, setFavouriteFailed] = useState(false);
  const [unfavouriteFailed, setUnfavouriteFailed] = useState(false);
  
  const [favourite, setFavourite] = useState(false);
  const [icon, setIcon] = useState("heart-outline");
  
  const onPress = async () => {
    if (!favourite) {
      setIcon("heart");
      setFavourite(true)
      const result = await locationApi.favLocation(location.location.location_id);
      
      if (!result.ok) {
        return setFavouriteFailed(true);
      }
      setFavouriteFailed(false);

    } else if (favourite) {
      setIcon("heart-outline");
      setFavourite(false);
      const result = await locationApi.unFavLocation(location.location.location_id);
      
      if (!result.ok) {
        return setUnfavouriteFailed(true);
      }
      setUnfavouriteFailed(false);

    }
  }

  return (
    <View>
      <FlatList
        data={location.location.location_reviews}
        keyExtractor={(review) => review.review_id.toString()}
        renderItem={({ item }) => (
          <ReviewCard
            location_id={location.location.location_id}
            review_id={item.review_id}
            review_user_id={item.review_user_id}
            review_overallrating={item.review_overallrating}
            review_pricerating={item.review_pricerating}
            review_qualityrating={item.review_qualityrating}
            review_clenlinessrating={item.review_clenlinessrating}
            review_body={item.review_body}
            likes={item.likes}
          />
        )}
        ListHeaderComponent={
          <View>
            <Image
              style={styles.image}
              tint="light"
              uri={location.location.photo_path}
            />
            <View style={styles.detailsContainer}>
              <Text style={styles.title}>
                {location.location.location_name}
              </Text>
              <Text style={styles.subTitle}>
                {location.location.location_town}
              </Text>
              <ErrorMessage error="error with favouriting" visible={favouriteFailed} />
        <ErrorMessage error="error with unfavouriting" visible={unfavouriteFailed} />
        
              <Button
                buttonStyle={{ backgroundColor: "#150e09" }}
                titleStyle={{
                  padding: 8,
                  fontSize: 20,
                  justifyContent: "center",
                  textAlign: "center",
                }}
                onPress={onPress}
                icon={
                  <MaterialCommunityIcons name={icon} color="#f50" size={35} />
                }
              />
              <View style={styles.userContainer}>
                <Button
                  buttonStyle={styles.button}
                  title="Add a Review"
                  color="secondary"
                  onPress={() => {
                    navigation.navigate(routes.REVIEW_EDIT, {
                      screen: "ReviewFormComponent",
                      params: { location: location.location.location_id },
                    });
                  }}
                />
              </View>
            </View>
          </View>
        }
      />
    </View>
  );
}

const styles = StyleSheet.create({
  detailsContainer: {
    padding: 20,
  },
  screen: {
    padding: 10,
    backgroundColor: colors.light,
  },
  image: {
    width: "100%",
    height: 300,
  },
  subTitle: {
    color: colors.secondary,
    fontWeight: "bold",
    fontSize: 20,
    marginVertical: 10,
  },
  title: {
    fontSize: 24,
    fontWeight: "500",
  },
  userContainer: {
    marginVertical: 40,
  },
  button: {
    backgroundColor: colors.primary,
    borderRadius: 25,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 15,
    width: "100%",
    marginVertical: 10,
  },
});

export default LocationDetailsScreen;
