import React, { useState } from "react";
import { StyleSheet, Image, View, Button } from "react-native";
import * as Yup from "yup";
import Popup from "reactjs-popup";

import Screen from "../components/Screen";
import {
  SuccessMessage,
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import authApi from "../api/auth";
import useAuth from "../auth/useAuth";
import updateApi from "../api/users";
import routes from "../navigation/routes";

const validationSchema = Yup.object().shape({
  first_name: Yup.string().required().label("First Name"),
  last_name: Yup.string().required().label("Last Name"),
  email: Yup.string().required().email().label("Email"),
  password: Yup.string().required().min(4).label("Password"),
});

function AccountDetailsScreen({ navigation }) {
  const auth = useAuth();
  const [postFailed, setPostFailed] = useState(false);
  const [postSuccess, setPostSuccess] = useState(false);

  const user = auth.user;
  const handleSubmit = async ({ first_name, last_name, email, password }) => {
    var userId = user.user_id;
    console.log(userId);

    const result = await updateApi.updateUser(
      first_name,
      last_name,
      email,
      password,
      userId
    );

    if (!result.ok) {
      return setPostFailed(true);
    }
    setPostFailed(false);
    setPostSuccess(true);
  };

  return (
    <Screen style={styles.container}>
      <Form
        initialValues={{
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email,
          password: user.password,
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <SuccessMessage
          success="Successfully updated account details"
          visible={postSuccess}
        />
        <ErrorMessage error="Issue with posting update" visible={postFailed} />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="account-box"
          name="first_name"
          placeholder="First Name"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="account-box"
          name="last_name"
          placeholder="Last Name"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="email"
          keyboardType="email-address"
          name="email"
          placeholder="Email"
          textContentType="emailAddress"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="lock"
          name="password"
          placeholder="Password"
          secureTextEntry
          textContentType="password"
        />
        <View style={styles.buttonsContainer}>
          <SubmitButton
            title="Save"
            color="secondary"
            onPress={() => navigation.navigate(routes.ACCOUNT_SCREEN)}
          />
        </View>
      </Form>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default AccountDetailsScreen;
