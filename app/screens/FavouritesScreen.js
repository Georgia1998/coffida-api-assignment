import React, { useState, useEffect } from "react";
import { FlatList, StyleSheet } from "react-native";

import Button from "../components/Button";
import Screen from "../components/Screen";
import Card from "../components/Card";
import colors from "../config/colors";
import routes from "../navigation/routes";

import AppText from "../components/Text";
import useAuth from "../auth/useAuth";

function FavouritesScreen({ navigation }) {
  const auth = useAuth();
  const [noFavouritesFound, setNoFavouritesFound] = useState(false);

  var favourites = auth.user.favourite_locations;

  useEffect(() => {
    if (!favourites) return setNoFavouritesFound(true);

    setNoFavouritesFound(false);
  });

  return (
    <>
      <Screen style={styles.screen}>
        {noFavouritesFound && (
          <>
            <AppText>No favourites found</AppText>
            <Button title="Retry" onPress={auth.user.favourite_locations} />
          </>
        )}
        <FlatList
          data={favourites}
          keyExtractor={(favourite) => favourite.location_id.toString()}
          renderItem={({ item }) => (
            <Card
              location_name={item.location_name}
              location_town={item.location_town}
              photo_path={item.photo_path}
              onPress={() => navigation.navigate(routes.LOCATION_DETAILS, item)}
            />
          )}
        />
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 20,
    backgroundColor: colors.light,
  },
});

export default FavouritesScreen;
