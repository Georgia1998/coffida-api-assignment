import React, { useEffect, useState } from "react";
import {
  FlatList,
  StyleSheet,
  View,
  TextInput,
  ScrollView,
} from "react-native";
import ActivityIndicator from "../components/ActivityIndicator";
import Button from "../components/Button";
import Card from "../components/Card";
import colors from "../config/colors";
import locationsApi from "../api/locations";
import routes from "../navigation/routes";
import Screen from "../components/Screen";
import AppText from "../components/Text";
import useApi from "../hooks/useApi";

function LocationsScreen({ navigation }) {
  const [search, setSearch] = useState("");

  const getListingsApi = useApi(locationsApi.getLocations);

  useEffect(() => {
    getListingsApi.request();
  }, []);

  const data = getListingsApi.data;
  console.log(data)
  const result = data.map((item) => {
    return {
      location_name: item.location_name,
      location_town: item.location_town,
      location_id: item.location_id,
      photo_path: item.photo_path,
      location_reviews: item.location_reviews,
    };
  });

  const filterList = (result) => {
    return result.filter(
      (listItem) =>
        listItem.location_name.toLowerCase().includes(search.toLowerCase()) ||
        listItem.location_town.toLowerCase().includes(search.toLowerCase())
    );
  };

  return (
    <>
      <ActivityIndicator visible={getListingsApi.loading} />
      <Screen style={styles.screen}>
        {getListingsApi.error && (
          <>
            <AppText>Couldn't retrieve the listings.</AppText>
            <Button title="Retry" onPress={getListingsApi.request} />
          </>
        )}
        <View>
          <TextInput
            style={styles.searchBar}
            onChangeText={(search) => setSearch(search)}
            placeholder="Search..."
          />
        </View>
        <View>
          <ScrollView>
            {filterList(result).map((listItem, index) => (
              <Card
                key={index}
                location_name={listItem.location_name}
                location_town={listItem.location_town}
                photo_path={listItem.photo_path}
                onPress={() => {
                  navigation.navigate(routes.LOCATION_DETAILS, {
                    screen: "LocationsDetails",
                    params: { location: listItem },
                  });
                }}
              />
            ))}
          </ScrollView>
        </View>
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 20,
    backgroundColor: colors.light,
  },
  searchBar: {
    fontSize: 20,
    margin: 10,
    width: "95%",
    height: 50,
    backgroundColor: colors.white,
    borderRadius: 25,
    flexDirection: "row",
    padding: 15,
    marginVertical: 10,
  },
});

export default LocationsScreen;
