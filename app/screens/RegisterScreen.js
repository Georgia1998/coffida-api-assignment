import React, { useState } from "react";
import { StyleSheet, Image } from "react-native";
import * as Yup from "yup";

import Screen from "../components/Screen";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import authApi from "../api/auth";
import useAuth from "../auth/useAuth";
import regApi from "../api/users";

const validationSchema = Yup.object().shape({
  first_name: Yup.string().required().label("First Name"),
  last_name: Yup.string().required().label("Last Name"),
  email: Yup.string().required().email().label("Email"),
  password: Yup.string().required().min(4).label("Password"),
});

function RegisterScreen() {
  const auth = useAuth();
  const [registerFailed, setRegisterFailed] = useState(false);

  const handleSubmit = async ({ first_name, last_name, email, password }) => {
    
    const result = await regApi.register(
      first_name,
      last_name,
      email,
      password
    );

    if (!result.ok) return setRegisterFailed(true);

    setRegisterFailed(false);
    var userId = result.data.id;
    console.log(userId);

    const LoginInfo = await authApi.login(email, password);
    var authToken = LoginInfo.data.token;

    const user = await authApi.getUser(userId, authToken);

    auth.logIn(user.data, authToken);
  };
  return (
    <Screen style={styles.container}>
      <Form
        initialValues={{
          first_name: "",
          last_name: "",
          email: "",
          password: "",
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <ErrorMessage error="Register Failed" visible={registerFailed} />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="account-box"
          name="first_name"
          placeholder="First Name"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="account-box"
          name="last_name"
          placeholder="Last Name"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="email"
          keyboardType="email-address"
          name="email"
          placeholder="Email"
          textContentType="emailAddress"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          icon="lock"
          name="password"
          placeholder="Password"
          secureTextEntry
          textContentType="password"
        />
        <SubmitButton title="Register" />
      </Form>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default RegisterScreen;
