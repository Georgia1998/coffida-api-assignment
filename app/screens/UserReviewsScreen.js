import React, { useState, useEffect } from "react";
import { FlatList, StyleSheet, Card, Text } from "react-native";

import Button from "../components/Button";
import Screen from "../components/Screen";
import colors from "../config/colors";
import * as Yup from "yup";

import AppText from "../components/Text";
import useAuth from "../auth/useAuth";
import { View } from "react-native";
import {
  SuccessMessage,
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import reviewApi from "../api/reviews";


 const validationSchema = Yup.object().shape({
    overall_rating: Yup.string().required().matches(/^[0-5]+$/, "Rating needs to be between 1-5").label("overall_rating"),
    price_rating: Yup.string().required().matches(/^[0-5]+$/, "Rating needs to be between 1-5").label("price_rating"),
    quality_rating: Yup.string().required().matches(/^[0-5]+$/, "Rating needs to be between 1-5").label("quality_rating"),
    clenliness_rating: Yup.string().required().matches(/^[0-5]+$/, "Rating needs to be between 1-5").label("clenliness_rating"),
    review_body: Yup.string().required().label("review_body"),
   
  });


function UserReviewsScreen({ navigation }) {
  const auth = useAuth();
  const [noReviewsFound, setNoReviewsFound] = useState(false);
  
  const reviews = auth.user.reviews;


  useEffect(() => {
    if (!reviews) return setNoReviewsFound(true);

    setNoReviewsFound(false);
  });
  const [postFailed, setPostFailed] = useState(false);
  const [postSuccess, setPostSuccess] = useState(false);
  
  //var location = location.route.params.params.location;

  const handleSubmit = async ({
    overall_rating,
    price_rating,
    quality_rating,
    clenliness_rating,
    review_body,
    location_id,
    review_id
  }) => {
    var overall_rating = parseInt(overall_rating);
    var price_rating = parseInt(price_rating);
    var quality_rating = parseInt(quality_rating);
    var clenliness_rating = parseInt(clenliness_rating);
    var location_id = parseInt(location_id);
    var review_id = parseInt(review_id)

    const result = await reviewApi.updateReview(
      overall_rating,
      price_rating,
      quality_rating,
      clenliness_rating,
      review_body,
      location_id,
      review_id
    );


    //item.location.location_id
    if (!result.ok) {
    return setPostFailed(true);
    }
    setPostFailed(false);
    setPostSuccess(true);

  };
  return (
    <>
      <Screen style={styles.screen}>
        {noReviewsFound && (
          <>
            <AppText>No favourites found</AppText>
            <Button title="Retry" onPress={auth.user.reviews} />
          </>
        )}
        <View>
          <FlatList
            data={reviews}
            keyExtractor={(review) => review.review.review_id.toString()}
            renderItem={({ item }) => (
                <Form
                initialValues={{
                  overall_rating: item.review.overall_rating.toString(),
                  price_rating: item.review.price_rating.toString(),
                  quality_rating: item.review.quality_rating.toString(),
                  clenliness_rating: item.review.clenliness_rating.toString(),
                  review_body: item.review.review_body,
                  review_id: item.review.review_id.toString(),
                location_id: item.location.location_id.toString(),
                  }}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
              >
                <View>
                 <SuccessMessage
                  success="Successfully updated review"
                  visible={postSuccess}
                />
                <ErrorMessage
                  error="Issue with posting update"
                  visible={postFailed}
                />
                </View>
                <Text field="location_id" style={{display: 'none'}} />
                <Text field="review_id" style={{display: 'none'}} />
                <FormField
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  name="overall_rating"
                  placeholder="Overall Rating"
                />
                <FormField
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  name="price_rating"
                  placeholder="Price Rating"
                />
                <FormField
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  name="quality_rating"
                  placeholder="Overall Rating"
                />
                <FormField
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="numeric"
                  name="clenliness_rating"
                  placeholder="Overall Rating"
                />
                <FormField
                  autoCapitalize="none"
                  autoCorrect={false}
                  name="review_body"
                  placeholder="Overall Rating"
                />
                <SubmitButton title="Submit Update" />
              </Form>
            )}
          />
        </View>
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 20,
    backgroundColor: colors.white,
  },
});

export default UserReviewsScreen;
