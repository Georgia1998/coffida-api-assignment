import React, { useState } from "react";
import { StyleSheet } from "react-native";
import * as Yup from "yup";

import Screen from "../../components/Screen";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../../components/forms";
import reviewApi from "../../api/reviews";


 const validationSchema = Yup.object().shape({
    overall_rating: Yup.string().required().matches(/^[0-5]+$/, "Rating needs to be between 1-5").label("overall_rating"),
    price_rating: Yup.string().required().matches(/^[0-5]+$/, "Rating needs to be between 1-5").label("price_rating"),
    quality_rating: Yup.string().required().matches(/^[0-5]+$/, "Rating needs to be between 1-5").label("quality_rating"),
    clenliness_rating: Yup.string().required().matches(/^[0-5]+$/, "Rating needs to be between 1-5").label("clenliness_rating"),
    review_body: Yup.string().required().label("review_body"),
   
  });
    

function UserReviewEditScreen(location,
    overall_rating,
    price_rating,
    quality_rating,
    clenliness_rating,
    review_body,) {
  const [postFailed, setPostFailed] = useState(false);
  console.log()
  console.log("edit Screen")
  const handleSubmit = async ({
    overall_rating,
    price_rating,
    quality_rating,
    clenliness_rating,
    review_body,
  }) => {
    var overall_rating = parseInt(overall_rating);
    var price_rating = parseInt(price_rating);
    var quality_rating = parseInt(quality_rating);
    var clenliness_rating = parseInt(clenliness_rating);

    const result = await reviewApi.updateReview(
      overall_rating,
      price_rating,
      quality_rating,
      clenliness_rating,
      review_body,
      location
    );

    console.log(result)

    if (!result.ok) return setPostFailed(true);
    setPostFailed(false);
  };
  return (
    <Screen style={styles.container}>
      <Form
        initialValues={{
          overall_rating: overall_rating,
          price_rating: price_rating,
          quality_rating: quality_rating,
          clenliness_rating: clenliness_rating,
          review_body: review_body,
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <ErrorMessage
          error="Issue with posting review"
          visible={postFailed}
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="numeric"
          name="overall_rating"
          placeholder="Overall Rating"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="numeric"
          name="price_rating"
          placeholder="Price Rating"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="numeric"
          name="quality_rating"
          placeholder="Overall Rating"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="numeric"
          name="clenliness_rating"
          placeholder="Overall Rating"
        />
        <FormField
          autoCapitalize="none"
          autoCorrect={false}
          name="review_body"
          placeholder="Overall Rating"
        />
        <SubmitButton title="Submit" />
      </Form>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default UserReviewEditScreen;
