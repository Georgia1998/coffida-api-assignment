import { create } from "apisauce";
import authStorage from "../auth/storage";
import cache from "../util/cache";

const client = create({
  baseURL: "http://192.168.0.23:3333/api/1.0.0/",
  //baseURL: "http://192.168.0.32:3333/api/1.0.0/",
  headers: { "Content-Type": "application/json" },
});

client.addAsyncRequestTransform(async (request) => {
  const authToken = await authStorage.getToken();
  if (!authToken) return;
  request.headers["X-Authorization"] = authToken;
});

const get = client.get;
client.get = async (url, params, axiosConfig) => {
  const response = await get(url, params, axiosConfig);

  if (response.ok) {
    cache.store(url, response.data);
    return response;
  }

  const data = await cache.get(url);
  return data ? { ok: true, data } : response;
};

export default client;
