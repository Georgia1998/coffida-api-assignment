import client from "./client";

const loc = "/location/";

const getLocations = (args) => {
  return client.get("/find");
};

const favLocation = (loc_id) =>
  client.post(loc + loc_id + "/favourite");

const unFavLocation = (loc_id) =>
  client.delete(loc + loc_id + "/favourite");

export default {
  getLocations,
  favLocation,
  unFavLocation,
};
