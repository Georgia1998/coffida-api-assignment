import client from "./client";

const register = (first_name, last_name, email, password) => 
  client.post("/user", { first_name, last_name, email, password });

const updateUser = (first_name, last_name, email, password, userId) =>
client.patch("/user/" + userId, {first_name, last_name, email, password});

const getUser = (userId) => 
client.get("/user/" + userId);

export default { register, updateUser, getUser };

