import client from "./client";

const loc = "/location/";
const rev = "/review/";

 const addReview = (
  overall_rating,
  price_rating,
  quality_rating,
  clenliness_rating,
  review_body,
  location,
) => 
  client.post(
    loc + location + "/review", {
    overall_rating,
    price_rating,
    quality_rating,
    clenliness_rating,
    review_body,
    }
  );

const updateReview = (
  overall_rating,
  price_rating,
  quality_rating,
  clenliness_rating,
  review_body,
  location_id,
  review_id
) =>
  client.patch(loc + location_id + rev + review_id, {
    overall_rating,
    price_rating,
    quality_rating,
    clenliness_rating,
    review_body,
  });

const deleteReview = (loc_id, rev_id) =>
  client.delete(loc + loc_id + rev + rev_id);

const getPhoto = (loc_id, rev_id) =>
  client.get(loc + loc_id + rev + rev_id + "/photo", {});

const addPhoto = (loc_id, rev_id) =>
  client.post(loc + loc_id + rev + rev_id + "/photo", {});

const deletePhoto = (loc_id, rev_id) =>
  client.delete(loc + loc_id + rev + rev_id);

const likeReview = (loc_id, rev_id) =>
  client.post(loc + loc_id + rev + rev_id + "/like");

const removeLike = (loc_id, rev_id) =>
  client.delete(loc + loc_id + rev + rev_id + "/like");

export default {
  addReview,
  updateReview,
  deleteReview,
  getPhoto,
  addPhoto,
  deletePhoto,
  likeReview,
  removeLike,
};
