import client from "./client";

const login = (email, password) =>
  client.post("/user/login", { email, password });

const getUser = (userId, authToken) => {
  client.setHeaders({ "X-Authorization": authToken });
  return client.get("/user/" + userId);
};

export default {
  login,
  getUser,
};
