import React, { useState } from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import Text from "./Text";
import Button from "../components/Button";
import colors from "../config/colors";
import ListItem from "../components/lists/ListItem";
import { AirbnbRating } from "react-native-ratings";
import Icon from "../components/Icon";
import routes from "../navigation/routes";

const UserReviewCard = ({
  location_name,
  location_id,
  review_user_id,
  review_overallrating,
  review_pricerating,
  review_qualityrating,
  review_clenlinessrating,
  review_body,
}) => {
  
  return (
    <View style={styles.card}>
      <View style={styles.detailsContainer}>
      <Text style={styles.title} numberOfLines={2}>
          {location_name}
        </Text>
        <AirbnbRating
          count={5}
          defaultRating={review_overallrating}
          reviews={["Terrible", "Bad", "Good", "Very Good", "Excellent"]}
          size={20}
          isDisabled={true}
        />
        <AirbnbRating
          count={5}
          defaultRating={review_pricerating}
          reviews={["Terrible", "Bad", "Good", "Very Good", "Excellent"]}
          size={20}
          isDisabled={true}
        /> 
        <AirbnbRating
          count={5}
          defaultRating={review_qualityrating}
          reviews={["Terrible", "Bad", "Good", "Very Good", "Excellent"]}
          size={20}
          isDisabled={true}
        />
        <AirbnbRating
          count={5}
          defaultRating={review_clenlinessrating}
          reviews={["Terrible", "Bad", "Good", "Very Good", "Excellent"]}
          size={20}
          isDisabled={true}
        />
        <Text style={styles.title} numberOfLines={2}>
          {review_body}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
    backgroundColor: colors.white,
    marginBottom: 20,
    overflow: "hidden",
  },
  detailsContainer: {
    padding: 20,
  },
  image: {
    width: "100%",
    height: 200,
  },
  subTitle: {
    color: colors.secondary,
    fontWeight: "bold",
  },
  title: {
    marginBottom: 7,
  },
  starStyle: {
    width: 100,
    height: 20,
    marginBottom: 20,
  },
});

export default UserReviewCard;
