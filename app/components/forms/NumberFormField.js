import React from "react";
import { useFormikContext } from "formik";
import NumericInput from 'react-native-numeric-input'
import ErrorMessage from "./ErrorMessage";

function AppFormField({ name, width, ...otherProps }) {
  const {
    setFieldTouched,
    setFieldValue,
    errors,
    touched,
    values,
  } = useFormikContext();

  return (
    <>
      <NumericInput
      value={values}
        onBlur={() => setFieldTouched(values)}
        onChange={(value) => setFieldValue(value)}
        width={width}
        {...otherProps}
      />
      <ErrorMessage error={errors[name]} visible={touched[name]} />
    </>
  );
}

export default AppFormField;
