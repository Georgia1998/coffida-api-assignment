import React, { useState } from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import Text from "./Text";
import colors from "../config/colors";
import ListItem from "../components/lists/ListItem";
import { AirbnbRating } from "react-native-ratings";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Button } from "react-native-elements";
import reviewApi from "../api/reviews";
import {
  ErrorMessage,
} from "../components/forms";

const ReviewCard = ({
  location_id,
  review_id,
  review_user_id,
  review_overallrating,
  review_pricerating,
  review_qualityrating,
  review_clenlinessrating,
  review_body,
  likes,
}) => {
  const [likeFailed, setlikeFailed] = useState(false);
  
  const [unlikeFailed, setUnlikeFailed] = useState(false);
  
  const [like, setLike] = useState(likes);
  const [icon, setIcon] = useState("heart-outline");
  
  var location_id = parseInt(location_id);
  const onPress = async () => {
    if (like == likes) {
      setIcon("heart");
      setLike(likes + 1);
      const result = await reviewApi.likeReview(location_id,review_id);
      
      if (!result.ok) {
        return setlikeFailed(true);
      }
      setlikeFailed(false);

    } else if (like == likes + 1) {
      setIcon("heart-outline");
      setLike(like - 1);
      const result = await reviewApi.removeLike(location_id,review_id);
      
      if (!result.ok) {
        return setUnlikeFailed(true);
      }
      setUnlikeFailed(false);

    }
  };

  return (
    <View style={styles.card}>
      <View style={styles.detailsContainer}>
        <Text style={styles.text}>Overall Rating</Text>
        <AirbnbRating
          count={5}
          defaultRating={review_overallrating}
          reviews={["Terrible", "Bad", "Good", "Very Good", "Excellent"]}
          size={20}
          isDisabled={true}
        />
        <Text style={styles.text}>Price Rating</Text>
        <AirbnbRating
          count={5}
          defaultRating={review_pricerating}
          reviews={["Terrible", "Bad", "Good", "Very Good", "Excellent"]}
          size={20}
          isDisabled={true}
        />
        <Text style={styles.text}>Quality Rating</Text>
        <AirbnbRating
          count={5}
          defaultRating={review_qualityrating}
          reviews={["Terrible", "Bad", "Good", "Very Good", "Excellent"]}
          size={20}
          isDisabled={true}
        />
        <Text style={styles.text}>Clenliness Rating</Text>
        <AirbnbRating
          count={5}
          defaultRating={review_clenlinessrating}
          reviews={["Terrible", "Bad", "Good", "Very Good", "Excellent"]}
          size={20}
          isDisabled={true}
        />
        <Text>Review: </Text>
        <Text style={styles.title} numberOfLines={2}>
          {review_body}
        </Text>
        <View style={styles.userContainer}>
          <ListItem
            image={require("../assets/coffee.jpg")}
            title={"User: " + review_user_id}
          />
        </View>
        <ErrorMessage error="error with liking" visible={likeFailed} />
        <ErrorMessage error="error with unliking" visible={unlikeFailed} />
        <Button
          title={like.toString()}
          buttonStyle={{ backgroundColor: "#150e09" }}
          titleStyle={{padding: 8,fontSize: 20,justifyContent: "center",
          textAlign: "center",}}
          onPress={onPress}
          icon={<MaterialCommunityIcons name={icon} color="#f50" size={35} />}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 15,
    backgroundColor: colors.light,
    marginBottom: 20,
    overflow: "hidden",
  },
  button: {
    backgroundColor: colors.primary,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    padding: 15,
    width: "100%",
    marginVertical: 10,
  },
  text: {
    justifyContent: "center",
    padding: 15,
    alignItems: "center",
    marginBottom: 7,
  },
  userContainer: {
    borderColor: "black",
    borderWidth: 2,
  },
  detailsContainer: {
    padding: 20,
  },
  image: {
    width: "100%",
    height: 200,
  },
  subTitle: {
    color: colors.secondary,
    fontWeight: "bold",
  },
  title: {
    marginBottom: 7,
  },
  starStyle: {
    width: 100,
    height: 20,
    marginBottom: 20,
  },
  countContainer: {
    alignItems: "center",
    padding: 10,
  },
  countText: {
    color: "#FF00FF",
  },
});

export default ReviewCard;
